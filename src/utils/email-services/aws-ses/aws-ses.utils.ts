import * as aws from 'aws-sdk';
import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';
import { EmailTemplate } from './email-template';

@Injectable()
export class AwsSES {
  private token: string;
  private secToken: string;
  private region: string;
  protected ses: any;

  constructor(private configService: ConfigService) {
    this.token = this.configService.get('AWS_ACCESS_KEY_ID');
    this.secToken = this.configService.get('AWS_SECRET_ACCESS_KEY');
    this.region = this.configService.get('AWS_REGION');
    this.ses = new aws.SES({
      accessKeyId: this.token,
      secretAccessKey: this.secToken,
      region: this.region,
    });
  }

  async awsMail(
    emailTo: string,
    emailFrom: string,
    name: string,
    phone: string,
  ) {
    const template = new EmailTemplate(name, phone);
    const params = {
      Destination: {
        ToAddresses: [emailTo],
      },
      Message: {
        Body: {
          Text: {
            Data: template.data,
          },
        },
        Subject: {
          Data: `Name is ${emailFrom}`,
        },
      },
      Source: 'apolin@algosolver.com',
    };

    return this.ses.sendEmail(params).promise();
  }
}
