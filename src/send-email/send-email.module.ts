import { Module } from '@nestjs/common';
import { AwsSES } from 'src/utils/email-services/aws-ses/aws-ses.utils';
import { SendEmailController } from './send-email.controller';
import { SendEmailService } from './send-email.service';

@Module({
  imports: [],
  controllers: [SendEmailController],
  providers: [SendEmailService, AwsSES],
})
export class SendEmailModule {}
