import { Injectable } from '@nestjs/common';
import { EmailDto } from './dto/send-email.dto';
import { ConfigService } from '@nestjs/config';
import { AwsSES } from 'src/utils/email-services/aws-ses/aws-ses.utils';

@Injectable()
export class SendEmailService {
  constructor(private configService: ConfigService, private awsSES: AwsSES) {}

  async sendEmail(emailDto: EmailDto): Promise<object> {
    try {
      const res = await this.awsSES.awsMail(
        'success@simulator.amazonses.com',
        emailDto.email,
        emailDto.name,
        emailDto.phone,
      );
      let response: object;
      if (res) {
        response = {
          code: 200,
          data: res,
        };
      }
      return response;
    } catch (error) {
      console.log(error);
      return { code: error?.statusCode || 400, data: error };
    }
  }
}
