export class EmailDto {
  email: string;
  name: string;
  phone: string;
}
