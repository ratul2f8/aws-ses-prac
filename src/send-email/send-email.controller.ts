import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { EmailDto } from './dto/send-email.dto';
import { SendEmailService } from './send-email.service';

@Controller('send-email')
export class SendEmailController {
  constructor(private readonly sendEmailService: SendEmailService) {}

  @ApiTags('SendEmail')
  @Post()
  async sendEmail(@Body() emailDto: EmailDto) {
    return this.sendEmailService.sendEmail(emailDto);
  }
}
